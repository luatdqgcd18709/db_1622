Use this repository with the slides posted on CMS.

## Some considerable notes
1. Teaching slides are a bit different from the one on FLM so be sure to update!
2. This course is fundamental to every developer since working with DB are essential in software developments & analytics. So please study it for you! It gonna be asked in job/internship interviews/tests for freshers.
3. Random SQL snippets on the Internet may outdated (or from other SQL dialects) lead to incompatibility so chose wisely.

## Preparation
1. For Server side, use [Microsoft® SQL Server® **2017** _Express_](https://www.microsoft.com/en-us/download/details.aspx?id=55994) by the normal installation method (Don't Docker)
2. For Client side (and Management tool), use [SQL Server Management Studio (SSMS)](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15) (be sure _newer 2017 version_ for compatibility)

Why ? DBMSs are similar but the course skeleton was designed & finalized by GW academic team to use Microsoft SQL Server **2017**, slides also were prepared best for SQL Server & SSMS tool user interface.

##  Issues
You might got trouble while installing or running query. Don't worry or give up too soon. 
1. Search over Internet first, pick relevant answers from trustworthy source (to avoid crappy websites with full of ads) like stackoverflow to try and solve.
2. In the case you couldn't find any proper help. Post/open issues here (this repository) so everybody (including me - Lecturer) can see, discuss and help. Openly raised issues benefit who may also got same trouble like you afterwards. _Don't be shy!_ Also _Never let your Ego stop you from Learning_

## Lesson Record Videos
Record Videos gonna be upload to YouTube and links shared from here:

1. [Day 1 video](https://drive.google.com/file/d/1aLACvSSgXrmtcy8ob-oJhkZZ1iqu-kil/view?usp=sharing) (Sorry for lengthy, I'll improve next time)
Don't forget to checkout exercises
2. [Day 2 video 1](https://drive.google.com/file/d/196t5wBxqXfWb4NtWR4ri-kcIv_l3CHY0/view?usp=sharing) and
[Day 2 video 2](https://drive.google.com/file/d/1wxO0zBFunnX_fxLMtbNyfuNnVZWTC9cy/view?usp=sharing) . Also checkout exercises from Day (still related to Day 2). SQL script uploaded too.
